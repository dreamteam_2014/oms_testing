package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


public class SupervisorHomePageTable extends CommonWebPage {
    @FindBy(id = "grid")
    private WebElement table;

    @FindBy(css = "#grid>tr:nth-of-type(2) td:nth-of-type(5) .edit_img")
    private WebElement editIcon;

    @FindBy(css = "#grid>tr:last-of-type .name")
    private WebElement newProductName;
    
    @FindBy(css = "#grid>tr:last-of-type td:nth-of-type(4)")
    private WebElement newProductPrice;

    @FindBy(css = "#grid>tr:last-of-type td:nth-of-type(3)")
    private WebElement newProductDescription;

    @FindBy(css = "#grid>tr:nth-of-type(2) td:nth-of-type(4)")
    private WebElement productPriceValue;

    String productID = "#grid>tr:last-of-type td:first-of-type";

    @FindBy(css = "#grid>tr:nth-of-type(3) td:nth-of-type(6) .delete_img")
    private WebElement deleteIcon;

    @FindBy(css = "#grid>tr:nth-of-type(3) td:first-of-type")
    private WebElement productIdForDeleting;


    public SupervisorHomePageTable(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(table));
        waitForEndOfAllAjaxes();
    }

    // Get product name from the last page of "Products" tab at SupervisorHomePage
    public String getProductName() {
        return newProductName.getText();
    }

    // Get product price from the last page of "Products" tab at SupervisorHomePage
    public String getProductPrice() {
        return newProductPrice.getText();
    }

    // Get product description from the last page of "Products" tab at SupervisorHomePage
    public String getProductDescription() {
        return newProductDescription.getText();
    }

    // Get product price from the first page of "Products" tab at SupervisorHomePage
    public String getNewPrice() {
        return productPriceValue.getText();
    }

    public EditProductPage goToEditProductPage() {
        editIcon.click();

        webWait.until(ExpectedConditions.alertIsPresent());
        alertAccept();

        return new EditProductPage(webDriver);
    }

    // Get ID of the last product from the last page of "Products" tab at SupervisorHomePage
    public String getProductID() {
        List<WebElement> rowsOfTable = webDriver.findElements(By.cssSelector(productID));
        if(rowsOfTable.size()>0) {
            return rowsOfTable.get(rowsOfTable.size()-1).getText();
        }
        return "";
    }

    // Delete product from the 4th page of "Products" tab at SupervisorHomePage
    public SupervisorHomePageTable deleteProduct() {
        deleteIcon.click();

        while (checkAlert()){
            alertAccept();
        }

        return new SupervisorHomePageTable(webDriver);
    }

    // Get ID of the product from the 4th page of "Products" tab at SupervisorHomePage
    public String getIdOfProductForDeleting() {
        return productIdForDeleting.getText();
    }
}
