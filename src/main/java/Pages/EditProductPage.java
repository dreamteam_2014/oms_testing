package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class EditProductPage extends CommonWebPage {

    @FindBy(id = "price")
    public WebElement fieldPrice;

    @FindBy(id = "update_product")
    public WebElement buttonUpdate;

    public EditProductPage(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(buttonUpdate));
        waitForEndOfAllAjaxes();
    }
    public void setNewPrice(String newPrice){
        fieldPrice.clear();
        fieldPrice.sendKeys(newPrice);
    }
    public SupervisorHomePage returnToSupervisorHomePage(){
        buttonUpdate.click();

        webWait.until(ExpectedConditions.alertIsPresent());
        alertAccept();

        return new SupervisorHomePage(webDriver);
    }
}
