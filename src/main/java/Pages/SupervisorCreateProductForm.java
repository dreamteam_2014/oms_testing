package Pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SupervisorCreateProductForm extends CommonWebPage {

    @FindBy(id = "info")
    private WebElement createProductForm;

    @FindBy(id = "product_name")
    private WebElement productName;

    @FindBy(id = "price")
    private WebElement productPrice;

    @FindBy(id = "description")
    private WebElement productDescription;

    @FindBy(id = "post_form")
    private WebElement buttonCreate;

    public SupervisorCreateProductForm(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(buttonCreate));
        waitForEndOfAllAjaxes();
    }

    public void setProductName(String name){
        productName.sendKeys(name);
    }
    public void setProductPrice(String price){
        productPrice.sendKeys(price);
    }
    public void setProductDescription(String description){
        productDescription.sendKeys(description);
    }

    public SupervisorHomePage returnToSupervisorHomePage(){
        buttonCreate.click();

        webWait.until(ExpectedConditions.alertIsPresent());
        alertAccept();

        return new SupervisorHomePage(webDriver);
    }
}
