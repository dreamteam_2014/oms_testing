package Pages;


import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CustomerOrderDetail extends CommonWebPage {

    private static final String STATE_SELECTOR_BY_MERCH ="merch";
    @FindBy(xpath = "//input[@id='preferable_date']")
    private WebElement fieldPreferableDeliveryDate;
    @FindBy(xpath = "//select[@id='credit_card_options']")
    private WebElement selectCard;
    @FindBy(xpath = "//input[@id='credit_card_number']")
    private WebElement cardNumber;
    @FindBy(xpath = "//input[@id='cvv2_number']")
    private WebElement cvv2Code;
    @FindBy(xpath = ".//input[@id='expire_date']")
    private WebElement expireDate;
    @FindBy(xpath = ".//*[@id='ui-datepicker-div']/div[2]/button[2]")
    private WebElement buttonDoneExpire;
    @FindBy(xpath = ".//input[@id='save_order']")
    private WebElement buttonSaveOrder;
    @FindBy(xpath = ".//input[@id='add_order']")
    private WebElement buttonOrder;
    @FindBy(xpath = "//input[@value=\"Add Item\"]")
    private WebElement buttonAddItem;
    @FindBy(xpath = "//*[@id='menu']/li[2]/a")
    private WebElement buttonMenuOrder;
    @FindBy(xpath = ".//*[@id='order_error']/p")
    private WebElement notificationMessage;
    @FindBy(xpath = ".//*[@id='ui-datepicker-div']/div[2]/button[2]")
    private WebElement buttonDoneInCalendar;
    private Alert alert;

    @FindBy(xpath = ".//select[@id='assignee']")
    private WebElement selectAssignerBy;

    By tableWithCalendar = By.xpath(".//*[@id='ui-datepicker-div']");

    // Constructor
    public CustomerOrderDetail(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(buttonAddItem));
        waitForEndOfAllAjaxes();

    }

    public AddItemPage toItemPage() {
        buttonAddItem.click();
        return new AddItemPage(webDriver);
    }

    public void setVisaCard(String visaNumber){
        choseSelectorOption(selectCard,"Visa");
        webWait.until(ExpectedConditions.visibilityOf(cardNumber));
        cardNumber.sendKeys(visaNumber);
    }

    public void setCVV2Code(String cvv2CodeNumber) {
        cvv2Code.sendKeys(cvv2CodeNumber);
    }

    public void setExpireDate() {
        expireDate.click();
        buttonDoneInCalendar.click();
    }

    public CustomerOrderDetail saveOrder(){
        new WebDriverWait(webDriver,4).until(ExpectedConditions.invisibilityOfElementLocated(tableWithCalendar));
        buttonSaveOrder.click();
        if(checkAlert()){
           alertAccept();
        }
      return new CustomerOrderDetail(webDriver);
    }

    public CustomerHomePage sendOrder(){
        buttonOrder.click();
        if(checkAlert()){
            alertAccept();
        }
        return new CustomerHomePage(webDriver);
    }

    public CustomerHomePage menuOrder() {
        buttonMenuOrder.click();
        return new CustomerHomePage(webDriver);
    }

    public void ActionSetFieldInOrder(String VisaNumber, String CVV2Code) {
        setVisaCard(VisaNumber);
        setCVV2Code(CVV2Code);
        choseSelectorOption(selectAssignerBy,STATE_SELECTOR_BY_MERCH);
        setExpireDate();
    }


    public String getAlertText() {
        alert = webDriver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        return alertText;
    }

    public void saveOrderTest() {
        new WebDriverWait(webDriver,4).until(ExpectedConditions.invisibilityOfElementLocated(tableWithCalendar));
        buttonSaveOrder.click();
    }
}