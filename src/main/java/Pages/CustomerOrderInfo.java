package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class CustomerOrderInfo extends CommonWebPage {

    @FindBy(xpath = "//*[@id='grid']/tr[2]/td[4]")
    WebElement colDimension;
    By tableIdWithRow = By.xpath("//*[@id=\"grid\"][count(tr)>0]");
    @FindBy(xpath = "//table/tr[2]/td[2]")
    private WebElement colName;
    @FindBy(xpath = "//tr[2]/td[span[@class='price']]")
    private WebElement colPrice;
    @FindBy(xpath = "//*[@id='grid']//input[@class='quantity']")
    private WebElement colQuantity;

    public CustomerOrderInfo(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOfElementLocated(tableIdWithRow));
        waitForEndOfAllAjaxes();
    }

    public String getActualItemName() {
        return colName.getText();
    }

    public String getActualPrice() {
        return colPrice.getText();
    }

    public String getActualQuantity() {
        return colQuantity.getAttribute("value");
    }

    public String getActualDimension() {
        return colDimension.getText();
    }
}
