package Pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class AddItemPage extends CommonWebPage {
    By divOntheAddItemPage = By.xpath(".//div[@id='exampleModal']");
    @FindBy(xpath = "//table[@id=\"grid_1\"]/tr[1]")
    private WebElement rowChoseItem;
    @FindBy(xpath = "//input[@id='add_item']")
    private WebElement buttonAddItem;
    @FindBy(xpath = "//input[@id=\"done\"]")
    private WebElement buttonDone;


    @FindBy(xpath = "//input[@id='item']")
    private WebElement fieldItem;
    @FindBy(xpath = "//input[@id='quantity']")
    private WebElement quantityOfItem;
    @FindBy(xpath = "//select[@id='dimension']")
    private WebElement selectDimension;


    /**
     * Clickable tag's <td> used for select item
     */
    @FindBy(xpath = ".//*[@id='4']/td[1]")
    private WebElement rowContainNameItem;
    @FindBy(css="#grid_1 > tr:last-of-type")
    private WebElement rowContainLastProduct;

    // Constructor page
    public AddItemPage(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOfElementLocated(divOntheAddItemPage));
        waitForEndOfAllAjaxes();
    }

    public AddItemPage addItem() {
        rowContainNameItem.click();
        webWait.until(ExpectedConditions.elementToBeClickable(buttonAddItem));
        buttonAddItem.click();
        return new AddItemPage(webDriver);

    }

    public AddItemPage addLastItem(){
        rowContainLastProduct.click();
        webWait.until(ExpectedConditions.elementToBeClickable(buttonAddItem));
        buttonAddItem.click();
        return new AddItemPage(webDriver);
    }

    public String getExpectedName() {
        return getTextFromElement("#item");
    }

    public String getExpectedPrice() {
        return getTextFromElement("#price");
    }

    public CustomerOrderDetail addChosenItemToOrder() {
        buttonDone.click();
        return new CustomerOrderDetail(webDriver);
    }

    public String getQuantity() {
        if (getTextFromElement("#quantity").equals("")) {
           return quantityOfItem.getAttribute("placeholder");
        } else {
           return getTextFromElement("#quantity");
        }
    }

    /**
     *
     *    Only number can be entered to "quantity" field
     *   if NaN entered to the field ,  value must be change to default value = 1
     * @param currentValue
     */
    private void setQuantity(String currentValue) {
        quantityOfItem.click();
        quantityOfItem.sendKeys(currentValue);
        quantityOfItem.sendKeys(Keys.TAB);
        // used for only into the current page
        waitForEndOfAllAjaxes();
    }

    // Get value from table (if cannot get from webdrive)
    public String getTextFromElement(String cssLocator) {
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        return (js.executeScript("return $(\"" + cssLocator + "\").val();")).toString();
    }

    public String getExpectedDimensions() {
        return new Select(selectDimension).getFirstSelectedOption().getText();
    }

    public String[] checkQuantityChange(String[] quantity) {
        String[] arrayExpected = new String[quantity.length];

        for (int count = 0;count<=quantity.length-1;count++) {
            setQuantity(quantity[count]);
            arrayExpected[count] = getQuantity();
            quantityOfItem.sendKeys(Keys.CONTROL+"a");
            quantityOfItem.sendKeys(Keys.DELETE);


        }
        return arrayExpected;
    }

}

