package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitWebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;


public class SupervisorHomePage extends CommonWebPage {

    public SupervisorHomePageTable supervisorHomePageTable;
    @FindBy(xpath = "//input[@value=\"Create Product\"]")
    private WebElement createProductButton;
    @FindBy(id = "last")
    private WebElement buttonLastPage;
    @FindBy(id = "name_options")
    private WebElement dropDownName;
    @FindBy(id = "name_input")
    private WebElement textBoxForDropDownName;
    @FindBy(id = "apply_button")
    private WebElement applyButton;
    private String optionContainsOfFilterName = "contains";
    @FindBy(id = "next")
    private WebElement buttonNextPage;
    @FindBy(id = "page")
    private WebElement numberOfCurrentPage;
    @FindBy(id = "pages_amount")
    private WebElement pagesAmount;


    /**
     * Class constructor
     *
     * @param driver
     */
    public SupervisorHomePage(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(createProductButton));
        supervisorHomePageTable = new SupervisorHomePageTable(driver);
    }

    public SupervisorCreateProductForm goToCreateProductForm() {
        createProductButton.click();
        return new SupervisorCreateProductForm(webDriver);
    }

    // Choose option "contains" in drop-down "Name"
    public void chooseFilterOfProductsByNameWithOptionContains() {
        Select selectOption = new Select(dropDownName);
        selectOption.selectByVisibleText(optionContainsOfFilterName);
    }

    // Input requested value for filtration to the text-box for drop-down "Name" and apply the filter
    public SupervisorHomePageTable inputRequestValueForFiltrationByName(String option) {
        textBoxForDropDownName.sendKeys(option);
        applyButton.click();
        return new SupervisorHomePageTable(webDriver);
    }

    // Go to the last page of "Products" tab at SupervisorHomePage to verify the presence of new created product.
    public SupervisorHomePageTable goToLastPageIfAvailable() {
        webDriver.findElement(By.id("last"));
        if (Integer.parseInt(pagesAmount.getText())>1){
            buttonLastPage.click();
        }
        return new SupervisorHomePageTable(webDriver);
    }

    public SupervisorHomePageTable goToNextPage() {
        if (Integer.parseInt(numberOfCurrentPage.getText())!=0){
            buttonNextPage.click();
        }
        return new SupervisorHomePageTable(webDriver);
    }
}
