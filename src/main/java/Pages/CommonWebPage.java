package Pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Class for representation of all web pages in our web app
 */
class CommonWebPage {
    protected WebDriver webDriver;
    protected WebDriverWait webWait;
    public static final int TIME_WAIT_FOR =20;

//
    /**
     * Logout link
     */
    @FindBy(css = "a[onclick]")
    private WebElement logoutLink;

    /**
     * Info icon
     */
    @FindBy(xpath = ".//*[@id='menu']//img")
    protected WebElement infoIcon;

    public CommonWebPage(WebDriver driver) {
        //Bounding of the local webDriver with external driver instance(constructor parameter)
        webDriver = driver;
        webWait = new WebDriverWait(webDriver, TIME_WAIT_FOR);
        //Initialization of all the WebElements marked with annotation @FindBy
        PageFactory.initElements(webDriver, this);
    }

    public LoginPage logoutFromPage(){
        logoutLink.click();
        Alert logOutPrompt = webDriver.switchTo().alert();
        logOutPrompt.accept();
        return new LoginPage(webDriver);
    }

    /**
     * Go to info page by clicking info icon
     *
     * @return
     */
    public InfoPage goToInfoPage() {
        infoIcon.click();
        return new InfoPage(webDriver);
    }


    public void waitForEndOfAllAjaxes(){
        webWait.until(new ExpectedCondition<Boolean>() {

            @Override

            public Boolean apply(WebDriver driver) {
                return (Boolean)((JavascriptExecutor)driver).executeScript("return jQuery.active == 0");
            }

        });
    }

    //Method for chose drop-down list
     protected void choseSelectorOption(WebElement selector,String option){
                  new Select(selector).selectByVisibleText(option);
     }

    protected void alertAccept(){
        Alert alert = webDriver.switchTo().alert();
        alert.accept();

    }

    /**
     * //  if don't use this wait.
     *    Alert in the Chrome cannot switch to alert .
        // Alert starts on Chrome slowly then command switch() .
     * @return
     */
    public Boolean checkAlert() {
        try {
            new WebDriverWait(webDriver,2).until(ExpectedConditions.alertIsPresent());
            return true;
        } catch (TimeoutException e) {
            return false;
        }
    }
}

