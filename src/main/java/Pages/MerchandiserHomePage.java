package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import java.util.ArrayList;
import java.util.HashSet;


public class MerchandiserHomePage extends CommonWebPage {
    MerchandiserHomePageTable merchandiserHomePageTable;

    @FindBy (id = "next")
    private WebElement buttonNextPage;

    @FindBy (id = "pages_amount")
    private WebElement pagesAmount;

    @FindBy (id = "first")
    private WebElement buttonFirstPage;

    @FindBy (id = "status_options")
    private WebElement statusSelection;


    @FindBy (id = "apply_button")
    private WebElement applyButton;

    Select selectOrdersStatus;
    /**
     * Constructor
     */
    public MerchandiserHomePage(WebDriver driver) {
        super(driver);
        webWait.until(ExpectedConditions.visibilityOf(statusSelection));
        selectOrdersStatus = new Select(statusSelection);
        merchandiserHomePageTable = new MerchandiserHomePageTable(driver);
    }

    //Method returns the array of enable filters is order's status selector
    public ArrayList<String> getSelectOrdersStatus(){
        ArrayList<String> selectOptionsTextList = new ArrayList<String>();
        for (int i=0; i< selectOrdersStatus.getOptions().size();i++){
            selectOptionsTextList.add(selectOrdersStatus.getOptions().get(i).getText());
        }
        return selectOptionsTextList;
    }

    //Method returns the array of merchandiser main table columns
    public ArrayList getTableHeader(){
        return merchandiserHomePageTable.getTableHeader();
    }

    //Method returns the list of orders' types from merchandiser main table
    public HashSet getOrdersStatuses(){
        HashSet<String> existOrdersStatuses = new HashSet<String>();

        if (Integer.parseInt(pagesAmount.getText())!=0){
            for(int j=1; j<=Integer.parseInt(pagesAmount.getText());j++) {
                existOrdersStatuses.addAll(merchandiserHomePageTable.getOrdersStatuses());
                buttonNextPage.click();
                merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);
            }
        }
        return existOrdersStatuses;
    }

    public void setOrdersSelectByStatus(String status){
        selectOrdersStatus.selectByVisibleText(status);
        applyButton.click();
        merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);
        buttonFirstPage.click();
        merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);
    }

    //Method removes the filter on orders' statuses
    public void setSelectAll(){
        selectOrdersStatus.selectByVisibleText("All");
        applyButton.click();
        merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);
        buttonFirstPage.click();
        merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);
    }

    //Method returns the order's page with certain status
    public MerchandiserOrderPage getOrderPageWithStatus(String status){
        MerchandiserOrderPage currentOrderPage = null;
        setOrdersSelectByStatus(status);

        if (Integer.parseInt(pagesAmount.getText())!=0){
            currentOrderPage = merchandiserHomePageTable.getOrderPage();
        }
        return currentOrderPage;
    }

    public MerchandiserOrderPage getOrderPageById(String orderId){
        MerchandiserOrderPage currentOrderPage = null;
        if (Integer.parseInt(pagesAmount.getText())!=0){
            for(int j=1; j<=Integer.parseInt(pagesAmount.getText());j++) {
                currentOrderPage = merchandiserHomePageTable.getOrderPageById(orderId);
                if (currentOrderPage != null) {
                    break;
                }
                buttonNextPage.click();
                merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);
            }
        }
        return currentOrderPage;
    }

    //Method returns status of particular order
    public String getOrderStatusById(String id){
        String currentStatus = "";
        merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);

        if (Integer.parseInt(pagesAmount.getText())!=0){
            for(int j=1; j<=Integer.parseInt(pagesAmount.getText());j++) {
                currentStatus = merchandiserHomePageTable.getOrderStatusById(id);
                if(!currentStatus.equals("")){
                    break;
                }
                buttonNextPage.click();
                merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);
            }
        }
        return currentStatus;
    }

    public boolean findOrderById(String newOrderId) {
        boolean orderFound = false;

        if (Integer.parseInt(pagesAmount.getText())!=0){
            for(int j=1; j<=Integer.parseInt(pagesAmount.getText());j++) {
                orderFound = merchandiserHomePageTable.ifOrderExistsById(newOrderId);
                if (orderFound) {
                    break;
                }
                buttonNextPage.click();
                merchandiserHomePageTable = new MerchandiserHomePageTable(webDriver);
            }
        }
        return orderFound;
    }
}
