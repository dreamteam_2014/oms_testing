import Pages.LoginPage;
import TestData.GlobalVariables;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Basic class for test classes
 */
public class CommonTestBasis implements GlobalVariables{
    private String browserName = "";
    protected WebDriver driver;

    static final String Chrome_DriverPath ="..\\src\\test\\test_resources\\chromedriver.exe";

     /**
     * Class constructor
     */
    public CommonTestBasis (){

    }


    public void openBrowser(String startParameter, String browserName) throws MalformedURLException{
        if (startParameter.equalsIgnoreCase("local")){
            if (browserName.equalsIgnoreCase("Chrome")) {
                System.setProperty("webdriver.chrome.driver", Chrome_DriverPath);
                driver = new ChromeDriver();
            }
            else
                driver = new FirefoxDriver();
        }
        else {
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setBrowserName("firefox");
        //capability.setVersion("27.0.1");
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
        }
    }

    // Navigate to the project
    public LoginPage urlOpen(){
        driver.get(URL_TEST);
        return new LoginPage(driver);
    }

    public void logIn(String login, String password){
        LoginPage loginPage = urlOpen();
        loginPage.logInProcedure(login, password);
    }

    /**
     * Every time after test class , it closes browser
     */
    @AfterClass
    public void closeBrowser() {
        if (driver != null) {
            driver.quit();
        }
    }

    protected boolean listsHaveSameElements(ArrayList<String> expectedResult, ArrayList<String> actualResult){
        boolean found = false;

        if(actualResult.size()!=expectedResult.size()){
            return false;
        }
        else if(actualResult.containsAll(expectedResult)){
                found = true;
                }
        return found;
    }

    public WebDriver getDriver() {
        return driver;
    }
}
