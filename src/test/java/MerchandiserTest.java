import Pages.*;
import org.openqa.selenium.By;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import org.testng.asserts.SoftAssert;

public class MerchandiserTest extends CommonTestBasis{
    MerchandiserHomePage merchandiserHomePage;
    ArrayList<String> expectOptions = new ArrayList<String>(Arrays.asList("All","Pending","Ordered","Delivered"));
    String newOrderId = "";

    MerchandiserTest(){
    //    super("Chrome");
    }

    public String createNewOrder(CustomerHomePage customerHomePage){
        CustomerOrderDetail customerOrderDetail = customerHomePage.createNewOder();
        AddItemPage addItemPage = customerOrderDetail.toItemPage();
        addItemPage.addItem();
        customerOrderDetail = addItemPage.addChosenItemToOrder();
        customerOrderDetail.ActionSetFieldInOrder(VISA_CARD_NUMBER, CVV2_CODENUMBER);
        customerOrderDetail=customerOrderDetail.saveOrder();
        customerHomePage = customerOrderDetail.sendOrder();
        return customerHomePage.getLastOrderId();
    }

    public String createPendingOrder(){
        LoginPage loginPage = merchandiserHomePage.logoutFromPage();
        loginPage.logInProcedure(CUSTOMER_LOGIN, CUSTOMER_PASSWORD);
        CustomerHomePage customerHomePage = new CustomerHomePage(driver);
        String orderId = createNewOrder(customerHomePage);
        loginPage = customerHomePage.logoutFromPage();
        loginPage.logInProcedure(MERCHANDISER_LOGIN, MERCHANDISER_PASSWORD);
        merchandiserHomePage = new MerchandiserHomePage(driver);
        return orderId;
    }

    public void changeOrdersStatusFromPendingToOrdered(String orderId){
        MerchandiserOrderPage merchandiserOrderPage = merchandiserHomePage.getOrderPageById(orderId);
        MerchandiserOrderPage savedMerchandiserOrderPage = merchandiserOrderPage.saveOrderChanges();
        savedMerchandiserOrderPage.returnToHomePage();
    }

    public void changeOrdersStatusFromOrderedToDelivered(String orderId){
        MerchandiserOrderPage merchandiserOrderPage = merchandiserHomePage.getOrderPageById(orderId);
        MerchandiserOrderPage savedOrderPage = merchandiserOrderPage.changeOrdersStateToNextState();
        merchandiserHomePage = savedOrderPage.returnToHomePage();
    }

    @BeforeClass
    @Parameters(value = {"startParameter", "browserName"})
    public void merchandiserLogIn(@Optional("local") String startParameter, @Optional("Firefox") String browserName) {
        try {
            openBrowser(startParameter, browserName);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        logIn(MERCHANDISER_LOGIN, MERCHANDISER_PASSWORD);
        merchandiserHomePage = new MerchandiserHomePage(driver);
    }

    //Test log in as a merchandiser
    @Test(priority = 1)
    public void testLogInAsMerchandiser() {
        InfoPage myInfo = merchandiserHomePage.goToInfoPage();
        String infoRole = myInfo.getRole();

        assertEquals(MERCHANDISER_ROLE, infoRole, "Displayed role is wrong.");
    }

    //Check all expected filters exist in order's status selector
    @Test(priority = 2)
    public void testTypesOfOrdersFilterHas() {
        ArrayList<String> actualSelectOptions = merchandiserHomePage.getSelectOrdersStatus();
        assertTrue(listsHaveSameElements(expectOptions, actualSelectOptions), "Orders' types in selector differ from expected.\nExpected: "+expectOptions+" \nActual: "+ actualSelectOptions);
    }

    //Check all expected columns headers are in merchandiser home page
    @Test(priority = 3)
    public void testMerchandiserHomePageTableHeader() {
        ArrayList<String> expectedListTitles = new ArrayList<String>(Arrays.asList("Order id", "Order number", "Customer", "Status", "Amount", "Assignee", "Edit"));
        ArrayList<String> actualListTitles = merchandiserHomePage.getTableHeader();

        assertTrue(listsHaveSameElements(expectedListTitles, actualListTitles), "Columns in table header differ from expected.\nExpected: "+expectedListTitles+"\nActual: "+actualListTitles);
    }

    //Check orders with unexpected statuses not displayed in merchandiser home page
    @Test(priority = 4)
    public void testOrdersOfWhatStatusMerchandiserSees() {
        ArrayList<String> expectedOrdersStatuses = new ArrayList<String>(Arrays.asList("Pending","Ordered","Delivered"));
        HashSet<String> actualOrdersStatuses = merchandiserHomePage.getOrdersStatuses();

        SoftAssert softAssert = new SoftAssert();
        for (String currentItem : actualOrdersStatuses) {
            softAssert.assertTrue(expectedOrdersStatuses.contains(currentItem), "Orders with status '"+ currentItem +"' should not be shown in merchandiser home page.");
        }
        softAssert.assertAll();
    }

    @Test(groups = {"selectionOfOrdersByStatus"}, priority = 5)
    public void testPendingOrdersSelectionWorksProperly(){
        newOrderId = createPendingOrder();
        merchandiserHomePage.setOrdersSelectByStatus("Pending");
        boolean pendingOrderIsPresentAfterSelection = merchandiserHomePage.findOrderById(newOrderId);
        assertTrue(pendingOrderIsPresentAfterSelection, "Selection of pending orders doesn't work properly. Order with id '"+newOrderId+"' hasn't been shown.");
    }

    @Test(groups = {"selectionOfOrdersByStatus"}, priority = 6)
    public void testSelectionOfOrdersWithStatusOrderedWorksProperly(){
        changeOrdersStatusFromPendingToOrdered(newOrderId);
        merchandiserHomePage.setOrdersSelectByStatus("Ordered");
        boolean orderedOrderIsPresentAfterSelection = merchandiserHomePage.findOrderById(newOrderId);
        assertTrue(orderedOrderIsPresentAfterSelection, "Selection of orders with status 'Ordered' doesn't work properly. Order with id '"+newOrderId+"' hasn't been shown.");
    }

    @Test(groups = {"selectionOfOrdersByStatus"}, priority = 7)
    public void testSelectionOfOrdersWithStatusDeliveredWorksProperly(){
        changeOrdersStatusFromOrderedToDelivered(newOrderId);
        merchandiserHomePage.setOrdersSelectByStatus("Delivered");
        boolean orderedOrderIsPresentAfterSelection = merchandiserHomePage.findOrderById(newOrderId);
        assertTrue(orderedOrderIsPresentAfterSelection, "Selection of orders with status 'Delivered' doesn't work properly. Order with id '"+newOrderId+"' hasn't been shown.");
    }

    //Check checkbox "Ordered" exists in order's page of "pending" order
    @Test(priority = 8)
    public void testCheckboxWithTextOrderedIsPresentInOrdersPageWithStatusPending() {
        String expectedText = "Ordered";
        boolean found = false;

        newOrderId = createPendingOrder();
        MerchandiserOrderPage orderPage = merchandiserHomePage.getOrderPageById(newOrderId);
        assertEquals(orderPage.getStatusCheckboxQuantity(), 1, "In merchandiser order page should be only one checkbox.");
        if (orderPage.getStatusCheckboxText().indexOf(expectedText) >= 0) {
            found = true;
        }
        assertTrue(found, "There is no checkbox with text 'Ordered' in merchandiser order's page.");
    }

    @Test(priority = 9)
    public void testChangeOrdersStatusFromPendingToOrderedWithExistingCheckbox(){
        String expectedStatus = "Ordered";
        String actualStatus;
        MerchandiserOrderPage orderPage = merchandiserHomePage.getOrderPageById(newOrderId);
        MerchandiserOrderPage savedOrderPage = orderPage.changeOrdersStateToNextState();
        merchandiserHomePage = savedOrderPage.returnToHomePage();
        actualStatus = merchandiserHomePage.getOrderStatusById(newOrderId);
        assertEquals(expectedStatus, actualStatus, "Change order's status from pending to ordered with existing checkbox does not work properly.");
    }

    //Check checkbox "Delivered" exists in order's page of "ordered" order
    @Test(priority = 10)
    public void testCheckboxWithTextDeliveredIsPresentInOrdersPageWithStatusOrdered() {
        String expectedText = "Delivered";
        boolean found = false;
        String newOrderId = createPendingOrder();
        changeOrdersStatusFromPendingToOrdered(newOrderId);

        MerchandiserOrderPage orderPage = merchandiserHomePage.getOrderPageWithStatus("Ordered");
        if (orderPage != null) {
            assertEquals(orderPage.getStatusCheckboxQuantity(), 1);
            if (orderPage.getStatusCheckboxText().indexOf(expectedText) >= 0) {
                found = true;
            }
            assertTrue(found, "There is no Delivered checkbox in merchandiser order's page.");
        }
    }

    //Check order with status "ordered" changes its status to "delivered" by checkbox
    @Test(dependsOnMethods = {"testCheckboxWithTextDeliveredIsPresentInOrdersPageWithStatusOrdered"})
    public void testChangeStateFromOrderedToDelivered() {
        String orderId = "";
        String expectedStatus = "Delivered";
        String actualStatus = "";
        MerchandiserOrderPage orderPage = merchandiserHomePage.getOrderPageWithStatus("Ordered");
        if (orderPage != null) {
            orderId = orderPage.getOrderId();
            MerchandiserOrderPage savedOrderPage = orderPage.changeOrdersStateToNextState();
            merchandiserHomePage = savedOrderPage.returnToHomePage();
            actualStatus = merchandiserHomePage.getOrderStatusById(orderId);
        }
        assertEquals(expectedStatus, actualStatus, "Change order's status from ordered to delivered does not work properly.");
    }

    //Return to merchandiser home page
    @AfterMethod
    public void returnToHomePage() {
        driver.findElement(By.xpath("//ul[@id=\"menu\"]/li/a[@title=\"manage_orders\"]")).click();
        merchandiserHomePage = new MerchandiserHomePage(driver);
    }

    @AfterGroups(groups = {"selectionOfOrdersByStatus"})
    public void afterTestingSelectionByOrdersStatus(){
        merchandiserHomePage.setSelectAll();
    }
}
