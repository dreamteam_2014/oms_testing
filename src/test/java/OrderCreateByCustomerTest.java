
import Pages.AddItemPage;
import Pages.CustomerHomePage;
import Pages.CustomerOrderDetail;
import Pages.CustomerOrderInfo;
import org.testng.Reporter;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;


import java.net.MalformedURLException;

import static org.testng.Assert.*;

public class OrderCreateByCustomerTest extends CommonTestBasis{
    private static final String EXPECTED_NOTIFICATION_MESSAGE = "Please select items and add them to the order.";
    private static final String goToCustomerHomePage = "http://itacademy.pythonanywhere.com/orders";
    private CustomerHomePage customerHomePage;
    private AddItemPage addItemPage;
    private String expectedName;
    private String expectedPrice;
    private String expectedQuantity;
    private String expectedDimension;
    private CustomerOrderDetail customerOrderDetailPage;
    private CustomerOrderInfo customerOrderInfoPage;
    private String[] dateSetQuantity = {"step", "20step"};
    private String[] actualQuantity;
    private String ExpectedValueQuantity = "1";

    OrderCreateByCustomerTest(){
    //    super("Chrome");
    }
    @BeforeClass
    @Parameters(value = {"startParameter", "browserName"})
    public void openSiteAndLogin(@Optional("local") String startParameter, @Optional("Firefox") String browserName) {
         try {
             openBrowser(startParameter, browserName);
         } catch (MalformedURLException e) {
             e.printStackTrace();
         }
         logIn(CUSTOMER_LOGIN, CUSTOMER_PASSWORD);
         customerHomePage = new CustomerHomePage(driver);
    }

    @BeforeMethod
    public void pages() {
        customerOrderDetailPage = customerHomePage.createNewOder();
    }

    @Test
    public void testAbilityCreateOrderWithOneItem() {
        addItemPage = customerOrderDetailPage.toItemPage();
        addItemPage = addItemPage.addItem();

        expectedName = addItemPage.getExpectedName();
        Reporter.log("\"TestAddItemToOrder(Firefox)\"");
        Reporter.log("ExpectedName : " + expectedName);

        expectedPrice = addItemPage.getExpectedPrice();
        Reporter.log("ExpectedPrice: " + expectedPrice);

        expectedQuantity = addItemPage.getQuantity();
        Reporter.log("ExpectedQuantity: " + expectedQuantity);

        expectedDimension = addItemPage.getExpectedDimensions();
        Reporter.log("ExpectedDimension: " + expectedDimension);

        System.out.print(" ExpectedName : " + expectedName + "\n");
        System.out.print(" ExpectedPrice : " + expectedPrice + "\n");
        System.out.print(" ExpectedQuantity : " + expectedQuantity + "\n");
        System.out.print(" ExpectedDimension : " + expectedDimension + "\n");

        customerOrderDetailPage = addItemPage.addChosenItemToOrder();
        customerOrderDetailPage.ActionSetFieldInOrder(VISA_CARD_NUMBER, CVV2_CODENUMBER);
        customerOrderDetailPage=customerOrderDetailPage.saveOrder();

        customerHomePage = customerOrderDetailPage.menuOrder();
        customerHomePage.setFilterOrderByOptionOrdersNumber();
        customerHomePage =customerHomePage.applyFilter();


        customerOrderInfoPage = customerHomePage.clickToEdit();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(customerOrderInfoPage.getActualItemName(), expectedName,
                                "Actual ItemName: " + customerOrderInfoPage.getActualItemName()
                                + " isn't equal Expected ItemName " + expectedName);
        Reporter.log("Actual Name: " + customerOrderInfoPage.getActualItemName() + "\n");

        softAssert.assertEquals(customerOrderInfoPage.getActualPrice(), expectedPrice,
                                "Actual ItemPrice: " + customerOrderInfoPage.getActualPrice()
                                + " isn't equal Expected ItemPrice " + expectedPrice);
        Reporter.log("Actual ItemPrice: " + customerOrderInfoPage.getActualPrice() + "\n");

        softAssert.assertEquals(customerOrderInfoPage.getActualQuantity(), expectedQuantity,
                                "Actual ItemQuantity: " + customerOrderInfoPage.getActualQuantity()
                                + " isn't equal Expected ItemQuantity " + expectedQuantity);
        Reporter.log("Actual ItemQuantity: " + customerOrderInfoPage.getActualQuantity() + "\n");

        softAssert.assertEquals(customerOrderInfoPage.getActualDimension(), expectedDimension,
                                "Actual ItemDimension: " + customerOrderInfoPage.getActualDimension()
                                + " isn't equal Expected ItemDimension " + expectedDimension);
        Reporter.log("Actual ItemDimension: " + customerOrderInfoPage.getActualDimension() + "\n");
        Reporter.log("Test :" + Reporter.getOutput() + Reporter.getCurrentTestResult());

        softAssert.assertAll();
    }

    @Test
    public void testAbilityToDeleteCreatedOrder() {
        customerHomePage = customerOrderDetailPage.menuOrder();
        customerHomePage.setFilterOrderByOptionCreate();
        customerHomePage.setFilterOrderByOptionOrdersNumber();
        customerHomePage = customerHomePage.applyFilter();

        String expectedNumberOrderToDel = customerHomePage.getLastOrderId();
        customerHomePage=customerHomePage.clickToDeleteOrder();
        String actualNumberOrderToDelete = customerHomePage.getLastOrderId();
        System.out.print("ExpectedNumberOrderToDel: " + expectedNumberOrderToDel + ":"
                + actualNumberOrderToDelete + "ActualNumberOfOrder");
        assertFalse(actualNumberOrderToDelete.equals(expectedNumberOrderToDel),
                "Created order wasn't deleted by customer");
    }

    @Test
    public void testWarningMessageIfCreateOrderWithoutAnyItem() {
        customerOrderDetailPage.ActionSetFieldInOrder(VISA_CARD_NUMBER, CVV2_CODENUMBER);
        customerOrderDetailPage.saveOrderTest();

        assertTrue(customerOrderDetailPage.checkAlert(),
                    "Warning message doesn't appear if customer creates an order without items");
        assertEquals(customerOrderDetailPage.getAlertText(), EXPECTED_NOTIFICATION_MESSAGE,
                    "Actual warning message :" + customerOrderDetailPage.getAlertText()
                    + "isn't equal expected warning: " + EXPECTED_NOTIFICATION_MESSAGE);
    }

    @Test
    public void testAbilityToChangeQuantityIfNotNumber(){
        addItemPage = customerOrderDetailPage.toItemPage();
        addItemPage.addItem();
        actualQuantity = addItemPage.checkQuantityChange(dateSetQuantity);
        SoftAssert softAssertTestQuantity = new SoftAssert();
        for(int count=0;count<=dateSetQuantity.length-1;count++){
            softAssertTestQuantity.assertEquals(actualQuantity[count], ExpectedValueQuantity,
                                                "Quantity value :" + "\"" + dateSetQuantity[count] + "\""
                                                + " didn't change to expected value: " + ExpectedValueQuantity + "\n");
        }
        softAssertTestQuantity.assertAll();
    }

    @AfterMethod
    public void toHomepage() {
        driver.get(goToCustomerHomePage);
        customerHomePage = new CustomerHomePage(driver);

    }
}
