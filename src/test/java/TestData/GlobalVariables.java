package TestData;


public interface GlobalVariables {
    public static final String URL_TEST ="http://itacademy.pythonanywhere.com";
    public static final String MERCHANDISER_LOGIN = "merch";
    public static final String MERCHANDISER_PASSWORD = "merch";
    public static final String MERCHANDISER_ROLE = "Merchandiser";
    public static final String SUPERVISOR_LOGIN = "supervisor";
    public static final String SUPERVISOR_PASSWORD = "supervisor";
    public static final String SUPERVISOR_ROLE = "Supervisor";
    public static final String CUSTOMER_LOGIN = "custTest";
    public static final String CUSTOMER_PASSWORD = "stop";
    public static final String CVV2_CODENUMBER = "123";
    public static final String VISA_CARD_NUMBER = "1234512345123456";
}
